package de.schegge.holidays;

import java.time.LocalDate;
import java.util.function.Function;

public interface DateFunction extends Function<Integer, LocalDate> {

}
