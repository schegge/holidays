package de.schegge.holidays;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class LocalizedHolidays extends Holidays {

  private final Holidays wrapped;
  private final ResourceBundle bundle;

  public LocalizedHolidays(Holidays wrapped, Locale locale) {
    this.wrapped = wrapped;
    bundle = ResourceBundle.getBundle("holidays", locale);
  }

  @Override
  public boolean isHoliday(LocalDate date) {
    return wrapped.isHoliday(date);
  }

  @Override
  public Map<LocalDate, String> getHolidays(int year) {
    return Collections.unmodifiableMap(new LocalizedMap(wrapped.getHolidays(year), bundle));
  }

  @Override
  public Optional<String> getHoliday(LocalDate date) {
    return wrapped.getHoliday(date).map(bundle::getString);
  }
}
