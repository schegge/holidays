package de.schegge.holidays;

import de.schegge.holidays.location.Locode;
import de.schegge.holidays.location.SubDivision;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;

import static java.util.Objects.requireNonNull;

public abstract class Holidays {

    public static final Locale IRELAND = new Locale("en", "IE");
    public static final Locale AUSTRIA = new Locale("de", "AT");

    private static final Map<String, HolidaysProvider> providers = new HashMap<>();

    static {
        ServiceLoader<HolidaysProvider> serviceLoader = ServiceLoader.load(HolidaysProvider.class);
        serviceLoader.forEach(provider -> providers.putIfAbsent(provider.getCountry().getCountry(), provider));
    }

    private static final Map<Object, MapHolidays> map = new HashMap<>();

    public abstract boolean isHoliday(LocalDate date);

    public abstract Map<LocalDate, String> getHolidays(int year);

    public abstract Optional<String> getHoliday(LocalDate date);

    public static Holidays in(String country) {
        return in(country, Locale.getDefault());
    }

    public static Holidays in(String country, Locale language) {
        return in(new Locale(language.getLanguage(), country), language);
    }

    public static Holidays in(Locale locale) {
        return in(locale, Locale.getDefault());
    }

    public static Holidays in(Locale locale, Locale language) {
        MapHolidays holidays = map.get(requireNonNull(locale));
        if (holidays != null) {
            return new LocalizedHolidays(holidays, language);
        }
        return new LocalizedHolidays(in(locale, getProvider(locale)), language);
    }

    private static MapHolidays in(Locale locale, HolidaysProvider provider) {
        return map.computeIfAbsent(locale, l -> new MapHolidays(provider.create(locale)));
    }

    public static Holidays in(SubDivision subDivision) {
        return in(subDivision, Locale.getDefault());
    }

    public static Holidays in(SubDivision subDivision, Locale language) {
        MapHolidays holidays = map.get(requireNonNull(subDivision));
        if (holidays != null) {
            return new LocalizedHolidays(holidays, language);
        }
        Locale country = subDivision.getCountry();
        return new LocalizedHolidays(in(subDivision, country, getProvider(country)), language);
    }

    private static MapHolidays in(SubDivision subDivision, Locale country, HolidaysProvider provider) {
        MapHolidays byCountry = in(country, provider);
        return map.computeIfAbsent(subDivision, l -> new BackedHolidays(provider.create(subDivision), byCountry));
    }

    public static Holidays in(Locode locode) {
        return in(locode, Locale.getDefault());
    }

    public static Holidays in(Locode locode, Locale language) {
        MapHolidays holidays = map.get(requireNonNull(locode));
        if (holidays != null) {
            return holidays;
        }
        SubDivision subDivision = locode.getSubDivision();
        Locale country = subDivision.getCountry();
        HolidaysProvider provider = getProvider(country);
        Map<String, DateFunction> byLocode = provider.create(locode);
        MapHolidays mapHolidays = in(subDivision, country, provider);
        if (byLocode.isEmpty()) {
            return new LocalizedHolidays(mapHolidays, language);
        }
        BackedHolidays backedHolidays = new BackedHolidays(byLocode, mapHolidays);
        map.put(locode, backedHolidays);
        return new LocalizedHolidays(backedHolidays, language);
    }

    private static HolidaysProvider getProvider(Locale locale) {
        HolidaysProvider provider = providers.get(locale.getCountry());
        if (provider == null) {
            throw new IllegalArgumentException();
        }
        return provider;
    }
}
