package de.schegge.holidays;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

class LocalizedMap extends AbstractMap<LocalDate, String> {
  private final Map<LocalDate, String> wrapped;
  private final ResourceBundle bundle;

  private class LocalizedEntry extends SimpleImmutableEntry<LocalDate, String> {
    private LocalizedEntry(LocalDate key, String value) {
      super(key, value);
    }

    @Override
    public String getValue() {
      return bundle.getString(super.getValue());
    }
  }

  LocalizedMap(Map<LocalDate, String> wrapped, ResourceBundle bundle) {
    this.wrapped = wrapped;
    this.bundle = bundle;
  }

  @Override
  public int size() {
    return wrapped.size();
  }

  @Override
  public String get(Object key) {
    String value = wrapped.get(key);
    return value == null ? null : bundle.getString(value);
  }

  @Override
  public Set<Entry<LocalDate, String>> entrySet() {
    return wrapped.entrySet().stream().map(e -> new LocalizedEntry(e.getKey(), e.getValue()))
        .collect(Collectors.toSet());
  }
}
