package de.schegge.holidays;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

class MapHolidays extends Holidays {
    protected final Map<String, DateFunction> functions;

    public MapHolidays(Map<String, DateFunction> functions) {
        this.functions = functions;
    }

    @Override
    public boolean isHoliday(LocalDate date) {
        return getHolidays(date.getYear()).containsKey(date);
    }

    @Override
    public Optional<String> getHoliday(LocalDate date) {
        return Optional.ofNullable(getHolidays(date.getYear()).get(date));
    }

    @Override
    public Map<LocalDate, String> getHolidays(int year) {
        Map<LocalDate, String> result = new LinkedHashMap<>();
        for (Entry<String, DateFunction> entry : functions.entrySet()) {
            LocalDate key = entry.getValue().apply(year);
            if (key != null) {
                result.put(key, entry.getKey());
            }
        }
        return result;
    }
}