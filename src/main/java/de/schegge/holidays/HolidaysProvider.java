package de.schegge.holidays;

import de.schegge.holidays.location.Locode;
import de.schegge.holidays.location.SubDivision;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Map;

public interface HolidaysProvider {
    Map<String, DateFunction> create(Locale locale);

    Map<String, DateFunction> create(SubDivision locale);

    Map<String, DateFunction> create(Locode locale);

    Locale getCountry();

    static LocalDate todayOrBefore(DayOfWeek dayOfWeek, LocalDate baseDay) {
        LocalDate day = baseDay.with(dayOfWeek);
        return day.isAfter(baseDay) ? day.minusDays(7) : day;
    }

    static LocalDate todayOrAfter(DayOfWeek dayOfWeek, LocalDate baseDay) {
        LocalDate day = baseDay.with(dayOfWeek);
        return day.isAfter(baseDay) ? day : day.plusDays(7);
    }
}
