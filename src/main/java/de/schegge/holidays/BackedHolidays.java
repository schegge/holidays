package de.schegge.holidays;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

public class BackedHolidays extends MapHolidays {
	protected MapHolidays backedBy;

	public BackedHolidays(Map<String, DateFunction> functions, MapHolidays backedBy) {
		super(functions);
		this.backedBy = backedBy;
	}

	@Override
	public boolean isHoliday(LocalDate date) {
		return super.isHoliday(date) || backedBy.isHoliday(date);
	}

	@Override
	public Optional<String> getHoliday(LocalDate date) {
		Optional<String> result = super.getHoliday(date);
		return result.isPresent() ? result : backedBy.getHoliday(date);
	}

	@Override
	public Map<LocalDate, String> getHolidays(int year) {
		Map<LocalDate, String> holidays = backedBy.getHolidays(year);
		holidays.putAll(super.getHolidays(year));
		return holidays;
	}
}
