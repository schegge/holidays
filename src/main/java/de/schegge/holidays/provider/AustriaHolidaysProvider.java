package de.schegge.holidays.provider;

import static java.time.temporal.ChronoUnit.DAYS;

import de.schegge.holidays.Holidays;
import java.time.MonthDay;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.schegge.holidays.DateFunction;
import de.schegge.holidays.HolidaysProvider;
import de.schegge.holidays.location.Locode;
import de.schegge.holidays.location.SubDivision;

public class AustriaHolidaysProvider implements HolidaysProvider {
	private static final MonthDay BOXING_DAY = MonthDay.parse("--12-26");
	private static final MonthDay CHRISTMAS_DAY = MonthDay.parse("--12-25");
	private static final MonthDay STAATSFEIERTAG = MonthDay.parse("--05-01");
	private static final MonthDay NEW_YEARS_DAY = MonthDay.parse("--01-01");
	private static final MonthDay EPIPHANY_DAY = MonthDay.parse("--01-06");
	private static final MonthDay ASSUMPTION_DAY = MonthDay.parse("--08-15");
	private static final MonthDay NATIONAL_FEIERTAG = MonthDay.parse("--10-26");
	private static final MonthDay MARIAE_EMPFANEGNIS = MonthDay.parse("--12-08");
	private static final MonthDay ALLERHEILIGEN = MonthDay.parse("--11-01");

	@Override
	public Map<String, DateFunction> create(Locale locale) {
		Map<String, DateFunction> functions = new HashMap<>();
		functions.put("new-years-day", NEW_YEARS_DAY::atYear);
		functions.put("epiphany-day", EPIPHANY_DAY::atYear);
		functions.put("easter-monday", year -> EasterCache.gauss(year).plus(1, DAYS));
		functions.put("ascension-day", year -> EasterCache.gauss(year).plus(39, DAYS));
		functions.put("whit-monday", year -> EasterCache.gauss(year).plus(49, DAYS));
		functions.put("staatsfeiertag", STAATSFEIERTAG::atYear);
		functions.put("happy-cadaver", year -> EasterCache.gauss(year).plus(60, DAYS));
		functions.put("christmas-day", CHRISTMAS_DAY::atYear);
		functions.put("boxing-day", BOXING_DAY::atYear);
		functions.put("assumption-day", ASSUMPTION_DAY::atYear);
		functions.put("nationalfeiertag", NATIONAL_FEIERTAG::atYear);
		functions.put("immaculate-conception", MARIAE_EMPFANEGNIS::atYear);
		functions.put("all-saints", ALLERHEILIGEN::atYear);
		return functions;
	}

	@Override
	public Map<String, DateFunction> create(SubDivision subDivision) {
		return Collections.emptyMap();
	}

	@Override
	public Map<String, DateFunction> create(Locode locode) {
		return Collections.emptyMap();
	}

	@Override
	public Locale getCountry() {
		return Holidays.AUSTRIA;
	}
}
