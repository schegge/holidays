package de.schegge.holidays.provider;

import de.schegge.holidays.DateFunction;
import de.schegge.holidays.HolidaysProvider;
import de.schegge.holidays.location.GermanFederalState;
import de.schegge.holidays.location.GermanLocode;
import de.schegge.holidays.location.Locode;
import de.schegge.holidays.location.SubDivision;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.MonthDay;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class GermanHolidaysProvider implements HolidaysProvider {

    private static final MonthDay PARSED_NEW_YEARS_DAY = MonthDay.parse("--01-01");
    private static final MonthDay PARSED_LABOUR_DAY = MonthDay.parse("--05-01");
    private static final MonthDay PARSED_WORLD_CHILDREN_DAY = MonthDay.parse("--09-20");
    private static final MonthDay PARSED_EPIPHANY_DAY = MonthDay.parse("--01-06");
    private static final MonthDay PARSED_WOMEN_DAY = MonthDay.parse("--03-08");
    private static final MonthDay PARSED_ASSUMPTION_DAY = MonthDay.parse("--08-15");

    private static final MonthDay PARSED_AUGSBURG_HIGH_PEACE_FESTIVAL = MonthDay.parse("--08-08");
    private static final MonthDay PARSED_DAY_OF_GERMAN_UNITY_OLD = MonthDay.parse("--06-17");
    private static final MonthDay PARSED_DAY_OF_GERMAN_UNITY_NEW = MonthDay.parse("--10-03");
    private static final MonthDay PARSED_REFORMATION_DAY = MonthDay.parse("--10-31");
    private static final MonthDay PARSED_ALL_SAINTS_DAY = MonthDay.parse("--11-01");
    private static final MonthDay PARSED_PENANCE_DAY = MonthDay.parse("--11-22");
    private static final MonthDay PARSED_CHRISTMAS_DAY = MonthDay.parse("--12-25");
    private static final MonthDay PARSED_BOXING_DAY = MonthDay.parse("--12-26");

    private enum Holiday implements DateFunction {
        NEW_YEARS_DAY("new-years-day", PARSED_NEW_YEARS_DAY::atYear),
        PENANCE_DAY("penance-day", GermanHolidaysProvider::beforePenanceDay),
        WOMEN_DAY("women-day", PARSED_WOMEN_DAY::atYear),
        GOOD_FRIDAY("good-friday", year -> EasterCache.gauss(year).plusDays(-2)),
        EASTER_SUNDAY("easter-sunday", EasterCache::gauss),
        EASTER_MONDAY("easter-monday", year -> EasterCache.gauss(year).plusDays(1)),
        ASCENSION_DAY("ascension-day", year -> EasterCache.gauss(year).plusDays(39)),
        WHIT_SUNDAY("whit-sunday", year -> EasterCache.gauss(year).plusDays(49)),
        WHIT_MONDAY("whit-monday", year -> EasterCache.gauss(year).plusDays(50)),
        HAPPY_CADAVER("happy-cadaver", year -> EasterCache.gauss(year).plusDays(60)),
        LABOUR_DAY("labour-day", PARSED_LABOUR_DAY::atYear),
        EPIPHANY_DAY("epiphany-day", PARSED_EPIPHANY_DAY::atYear),
        DAY_OF_GERMAN_UNITY_OLD(
                "day-of-german-unity-old",
                year -> year < 1954 || year > 1990 ? null : PARSED_DAY_OF_GERMAN_UNITY_OLD.atYear(year)),
        DAY_OF_GERMAN_UNITY_NEW(
                "day-of-german-unity-new", year -> year < 1990 ? null : PARSED_DAY_OF_GERMAN_UNITY_NEW.atYear(year)),
        REFORMATION_DAY("reformation-day", PARSED_REFORMATION_DAY::atYear),
        ALL_SAINTS_DAY("all-saints-day", PARSED_ALL_SAINTS_DAY::atYear),
        ASSUMPTION_DAY("assumption-day", PARSED_ASSUMPTION_DAY::atYear),
        WORLD_CHILDREN_DAY("world-children-day", PARSED_WORLD_CHILDREN_DAY::atYear),
        CHRISTMAS_DAY("christmas-day", PARSED_CHRISTMAS_DAY::atYear),
        BOXING_DAY("boxing-day", PARSED_BOXING_DAY::atYear);

        private final DateFunction function;
        private final String name;

        Holiday(String key, DateFunction function) {
            this.name = key;
            this.function = function;
        }

        @Override
        public LocalDate apply(Integer year) {
            return function.apply(year);
        }

        public String getName() {
            return name;
        }
    }

    @Override
    public Map<String, DateFunction> create(Locale locale) {
        return Stream.of(Holiday.NEW_YEARS_DAY, Holiday.GOOD_FRIDAY, Holiday.EASTER_MONDAY, Holiday.ASCENSION_DAY,
                Holiday.WHIT_MONDAY, Holiday.LABOUR_DAY, Holiday.DAY_OF_GERMAN_UNITY_OLD, Holiday.DAY_OF_GERMAN_UNITY_NEW,
                Holiday.CHRISTMAS_DAY, Holiday.BOXING_DAY).collect(toMap(Holiday::getName, identity()));
    }

    @Override
    public Map<String, DateFunction> create(SubDivision subDivision) {
        return createStream(subDivision).collect(toMap(Holiday::getName, identity()));
    }

    private Stream<Holiday> createStream(SubDivision subDivision) {
        return switch (GermanFederalState.byIso(subDivision.getIso())) {
            case BB -> Stream.of(Holiday.EASTER_SUNDAY, Holiday.WHIT_SUNDAY, Holiday.REFORMATION_DAY);
            case BE -> Stream.of(Holiday.WOMEN_DAY);
            case BW, BY -> Stream.of(Holiday.EPIPHANY_DAY, Holiday.HAPPY_CADAVER, Holiday.ALL_SAINTS_DAY);
            case HE -> Stream.of(Holiday.EASTER_SUNDAY, Holiday.WHIT_SUNDAY, Holiday.HAPPY_CADAVER);
            case HB, HH, MV, NI -> Stream.of(Holiday.REFORMATION_DAY, Holiday.HAPPY_CADAVER, Holiday.ALL_SAINTS_DAY);
            case NW, RP -> Stream.of(Holiday.HAPPY_CADAVER, Holiday.ALL_SAINTS_DAY);
            case SH -> Stream.of(Holiday.REFORMATION_DAY);
            case SL -> Stream.of(Holiday.HAPPY_CADAVER, Holiday.ASSUMPTION_DAY);
            case SN -> Stream.of(Holiday.PENANCE_DAY, Holiday.REFORMATION_DAY);
            case ST -> Stream.of(Holiday.EPIPHANY_DAY, Holiday.REFORMATION_DAY);
            case TH -> Stream.of(Holiday.WORLD_CHILDREN_DAY, Holiday.REFORMATION_DAY);
        };
    }

    private static LocalDate beforePenanceDay(int year) {
        LocalDate baseDay = PARSED_PENANCE_DAY.atYear(year);
        LocalDate day = baseDay.with(DayOfWeek.WEDNESDAY);
        return day.isAfter(baseDay) ? day.minusDays(7) : day;
    }

    @Override
    public Map<String, DateFunction> create(Locode locode) {
        if (GermanLocode.byLocode(locode.getLocode()) == GermanLocode.AGB) {
            return singletonMap("augsburg-high-peace-festival", PARSED_AUGSBURG_HIGH_PEACE_FESTIVAL::atYear);
        }
        return emptyMap();
    }

    @Override
    public Locale getCountry() {
        return Locale.GERMANY;
    }
}
