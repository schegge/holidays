package de.schegge.holidays.provider;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import de.schegge.holidays.DateFunction;
import de.schegge.holidays.HolidaysProvider;
import de.schegge.holidays.location.Locode;
import de.schegge.holidays.location.SubDivision;
import de.schegge.holidays.location.UnitedKingdomCountryOrProvince;
import de.schegge.holidays.location.UnitedKingdomNation;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

public class UnitedKingdomProvider implements HolidaysProvider {

  private static final MonthDay PARSED_BOXING_DAY = MonthDay.parse("--12-26");
  private static final MonthDay PARSED_CHRISTMAS_DAY = MonthDay.parse("--12-25");
  private static final MonthDay PARSED_NEW_YEARS_DAY = MonthDay.parse("--01-01");
  private static final MonthDay PARSED_ST_PATRICKS_DAY = MonthDay.parse("--03-17");
  private static final MonthDay PARSED_ORANGEMANS_DAY = MonthDay.parse("--07-12");
  private static final MonthDay PARSED_ST_ANDREWS_DAY = MonthDay.parse("--11-30");

  public static final MonthDay PARSED_LAST_DAY_OF_AUGUST = MonthDay.of(Month.AUGUST, 31);
  public static final MonthDay PARSED_LAST_DAY_OF_MAY = MonthDay.of(Month.MAY, 31);
  public static final MonthDay PARSED_FIRST_DAY_OF_MAY = MonthDay.of(Month.MAY, 1);
  public static final MonthDay PARSED_FIRST_DAY_OF_AUGUST = MonthDay.of(Month.AUGUST, 1);

  private enum Holiday implements DateFunction {
    NEW_YEARS_DAY("new-years-day", PARSED_NEW_YEARS_DAY::atYear),
    ST_PATRICKS_DAY("st-patricks-day", PARSED_ST_PATRICKS_DAY::atYear),
    ST_ANDREWS_DAY("st-andrews-day", PARSED_ST_ANDREWS_DAY::atYear),
    ORANGEMANS_DAY("orangemans-day", PARSED_ORANGEMANS_DAY::atYear),
    MAY_DAY("may-day", year -> HolidaysProvider.todayOrAfter(DayOfWeek.MONDAY, PARSED_FIRST_DAY_OF_MAY.atYear(year))),
    GOOD_FRIDAY("good-friday", year -> EasterCache.gauss(year).plus(-2, DAYS)),
    EASTER_MONDAY("easter-monday", year -> EasterCache.gauss(year).plus(1, DAYS)),
    SPRING_BANK_HOLIDAY("spring-bank-holiday", year -> mondayBefore(year, PARSED_LAST_DAY_OF_MAY)),
    SUMMER_BANK_HOLIDAY("summer-bank-holiday", year -> mondayBefore(year, PARSED_LAST_DAY_OF_AUGUST)),
    SCT_SUMMER_BANK_HOLIDAY(
        "summer-bank-holiday", year -> HolidaysProvider.todayOrAfter(DayOfWeek.MONDAY, PARSED_FIRST_DAY_OF_AUGUST.atYear(year))),
    CHRISTMAS_DAY("christmas-day", PARSED_CHRISTMAS_DAY::atYear),
    BOXING_DAY("boxing-day", PARSED_BOXING_DAY::atYear);

    private DateFunction function;
    private String name;

    Holiday(String key, DateFunction function) {
      this.name = key;
      this.function = function;
    }

    @Override
    public LocalDate apply(Integer year) {
      return function.apply(year);
    }

    public String getName() {
      return name;
    }
  }

  @Override
  public Map<String, DateFunction> create(Locale locale) {
    return convert(Stream.of(Holiday.NEW_YEARS_DAY, Holiday.GOOD_FRIDAY, Holiday.CHRISTMAS_DAY, Holiday.BOXING_DAY));
  }

  @Override
  public Map<String, DateFunction> create(SubDivision subDivision) {
    UnitedKingdomNation nation = UnitedKingdomNation.byIso(subDivision.getIso());
    if (nation != null) {
      return convert(createByNation(nation));
    }
    UnitedKingdomCountryOrProvince state = UnitedKingdomCountryOrProvince.byIso(subDivision.getIso());
    if (state != null) {
      return convert(createByCountriesAndProvince(state));
    }
    throw new IllegalArgumentException("subdivision not supported: " + subDivision);
  }

  private Map<String, DateFunction> convert(Stream<Holiday> stream) {
    return stream.collect(toMap(Holiday::getName, identity()));
  }

  private Stream<Holiday> createByCountriesAndProvince(UnitedKingdomCountryOrProvince state) {
    switch (state) {
      case ENG:
        return Stream
            .of(Holiday.EASTER_MONDAY, Holiday.MAY_DAY, Holiday.SPRING_BANK_HOLIDAY, Holiday.SUMMER_BANK_HOLIDAY);
      case NIR:
        return Stream
            .of(Holiday.ST_PATRICKS_DAY, Holiday.EASTER_MONDAY, Holiday.ORANGEMANS_DAY, Holiday.SUMMER_BANK_HOLIDAY);
      case SCT:
        return Stream.of(Holiday.SCT_SUMMER_BANK_HOLIDAY, Holiday.ST_ANDREWS_DAY);
      case WLS:
        return Stream.of(Holiday.EASTER_MONDAY, Holiday.SUMMER_BANK_HOLIDAY);
      default:
        return Stream.empty();
    }
  }

  private Stream<Holiday> createByNation(UnitedKingdomNation nation) {
    if (nation != UnitedKingdomNation.EAW) {
      return Stream.empty();
    }
    return Stream.of(Holiday.EASTER_MONDAY, Holiday.SUMMER_BANK_HOLIDAY);
  }

  @Override
  public Map<String, DateFunction> create(Locode locale) {
    return Collections.emptyMap();
  }

  public Locale getCountry() {
    return Locale.UK;
  }

  static LocalDate mondayBefore(int year, MonthDay monthDay) {
    return HolidaysProvider.todayOrBefore(DayOfWeek.MONDAY, monthDay.atYear(year));
  }
}
