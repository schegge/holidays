package de.schegge.holidays.provider;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import de.schegge.holidays.DateFunction;
import de.schegge.holidays.Holidays;
import de.schegge.holidays.HolidaysProvider;
import de.schegge.holidays.location.Locode;
import de.schegge.holidays.location.SubDivision;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class IrelandProvider implements HolidaysProvider {

  private static final MonthDay PARSED_BOXING_DAY = MonthDay.parse("--12-26");
  private static final MonthDay PARSED_CHRISTMAS_DAY = MonthDay.parse("--12-25");
  private static final MonthDay PARSED_NEW_YEARS_DAY = MonthDay.parse("--01-01");
  private static final MonthDay PARSED_ST_PATRICKS_DAY = MonthDay.parse("--03-17");

  private static final Set<DayOfWeek> WEEKEND = EnumSet.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

  private enum Holiday implements DateFunction {
    NEW_YEARS_DAY("new-years-day", year -> moveFromWeekend(PARSED_NEW_YEARS_DAY.atYear(year))),
    ST_PATRICKS_DAY("st-patricks-day", year -> moveFromWeekend(PARSED_ST_PATRICKS_DAY.atYear(year))),
    MAY_DAY("may-day", year -> mondayAfter(year, MonthDay.of(Month.MAY, 1))),
    JUNE_BANK_HOLIDAY("june-bank-holiday", year -> mondayAfter(year, MonthDay.of(Month.JUNE, 1))),
    AUGUST_BANK_HOLIDAY("august-bank-holiday", year -> mondayAfter(year, MonthDay.of(Month.AUGUST, 1))),
    OCTOBER_BANK_HOLIDAY("october-bank-holiday", year -> mondayBefore(year, MonthDay.of(Month.OCTOBER, 31))),
    CHRISTMAS_DAY("christmas-day", PARSED_CHRISTMAS_DAY::atYear),
    BOXING_DAY("boxing-day", PARSED_BOXING_DAY::atYear);

    private DateFunction function;
    private String name;

    Holiday(String key, DateFunction function) {
      this.name = key;
      this.function = function;
    }

    @Override
    public LocalDate apply(Integer year) {
      return function.apply(year);
    }

    public String getName() {
      return name;
    }
  }

  @Override
  public Map<String, DateFunction> create(Locale locale) {
    return convert(Stream.of(Holiday.NEW_YEARS_DAY, Holiday.ST_PATRICKS_DAY, Holiday.MAY_DAY,
        Holiday.JUNE_BANK_HOLIDAY, Holiday.AUGUST_BANK_HOLIDAY, Holiday.OCTOBER_BANK_HOLIDAY,
        Holiday.CHRISTMAS_DAY, Holiday.BOXING_DAY));
  }

  private Map<String, DateFunction> convert(Stream<Holiday> stream) {
    return stream.collect(toMap(Holiday::getName, identity()));
  }

  private static LocalDate moveFromWeekend(LocalDate baseDay) {
    if (WEEKEND.contains(baseDay.getDayOfWeek())) {
      return HolidaysProvider.todayOrAfter(DayOfWeek.MONDAY, baseDay);
    }
    return baseDay;
  }

  private static LocalDate mondayBefore(int year, MonthDay monthDay) {
    return HolidaysProvider.todayOrBefore(DayOfWeek.MONDAY, monthDay.atYear(year));
  }

  private static LocalDate mondayAfter(int year, MonthDay monthDay) {
    return HolidaysProvider.todayOrAfter(DayOfWeek.MONDAY, monthDay.atYear(year));
  }

  @Override
  public Map<String, DateFunction> create(SubDivision subDivision) {
    return Collections.emptyMap();
  }

  @Override
  public Map<String, DateFunction> create(Locode locale) {
    return Collections.emptyMap();
  }

  @Override
  public final Locale getCountry() {
    return Holidays.IRELAND;
  }
}
