package de.schegge.holidays.provider;

import static de.schegge.holidays.HolidaysProvider.todayOrAfter;
import static de.schegge.holidays.HolidaysProvider.todayOrBefore;
import static java.time.DayOfWeek.THURSDAY;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import de.schegge.holidays.DateFunction;
import de.schegge.holidays.HolidaysProvider;
import de.schegge.holidays.location.Locode;
import de.schegge.holidays.location.SubDivision;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.MonthDay;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

public class UsaProvider implements HolidaysProvider {

  private static final MonthDay PARSED_CHRISTMAS_DAY = MonthDay.parse("--12-25");
  private static final MonthDay PARSED_NEW_YEARS_DAY = MonthDay.parse("--01-01");
  private static final MonthDay PARSED_MARTIN_LUTHER_KING_DAY = MonthDay.parse("--01-22");
  public static final MonthDay PARSED_PRESIDENTS_DAY = MonthDay.parse("--02-22");
  public static final MonthDay PARSED_MEMORIAL_DAY = MonthDay.parse("--05-31");
  public static final MonthDay PARSED_INDEPENDENTS_DAY = MonthDay.parse("--07-04");
  public static final MonthDay PARSED_LABOUR_DAY = MonthDay.parse("--09-01");
  public static final MonthDay PARSED_COLUMBUS_DAY = MonthDay.parse("--10-08");
  public static final MonthDay PARSED_VETERANS_DAY = MonthDay.parse("--11-11");
  public static final MonthDay PARSED_THANKSGIVING_DAY = MonthDay.parse("--11-29");

  private enum Holiday implements DateFunction {
    NEW_YEARS_DAY("new-years-day", PARSED_NEW_YEARS_DAY::atYear),
    MARTIN_LUTHER_KING_DAY("martin-luther-king-day", year -> mondayAfter(year, PARSED_MARTIN_LUTHER_KING_DAY)),
    PRESIDENTS_DAY("presidents-day", year -> mondayAfter(year, PARSED_PRESIDENTS_DAY)),
    MEMORIAL_DAY("memorial-day", year -> mondayBefore(year, PARSED_MEMORIAL_DAY)),
    INDEPENDENCE_DAY("independence-day", PARSED_INDEPENDENTS_DAY::atYear),
    LABOUR_DAY("labour-day", year -> mondayAfter(year, PARSED_LABOUR_DAY)),
    COLUMBUS_DAY("columbus-day", year -> mondayAfter(year, PARSED_COLUMBUS_DAY)),
    VETERANS_DAY("veterans-day", PARSED_VETERANS_DAY::atYear),
    THANKSGIVING_DAY("thanksgiving-day", year -> todayOrBefore(THURSDAY, PARSED_THANKSGIVING_DAY
        .atYear(year))),
    CHRISTMAS_DAY("christmas-day", PARSED_CHRISTMAS_DAY::atYear);
    private DateFunction function;
    private String name;

    Holiday(String key, DateFunction function) {
      this.name = key;
      this.function = function;
    }

    @Override
    public LocalDate apply(Integer year) {
      return function.apply(year);
    }

    public String getName() {
      return name;
    }
  }

  @Override
  public Map<String, DateFunction> create(Locale locale) {
    return convert(Stream.of(Holiday.NEW_YEARS_DAY, Holiday.MARTIN_LUTHER_KING_DAY, Holiday.PRESIDENTS_DAY,
        Holiday.MEMORIAL_DAY, Holiday.INDEPENDENCE_DAY, Holiday.LABOUR_DAY, Holiday.COLUMBUS_DAY,
        Holiday.VETERANS_DAY, Holiday.THANKSGIVING_DAY, Holiday.CHRISTMAS_DAY));
  }

  private Map<String, DateFunction> convert(Stream<Holiday> stream) {
    return stream.collect(toMap(Holiday::getName, identity()));
  }

  private static LocalDate mondayBefore(int year, MonthDay monthDay) {
    return todayOrBefore(DayOfWeek.MONDAY, monthDay.atYear(year));
  }

  private static LocalDate mondayAfter(int year, MonthDay monthDay) {
    return todayOrAfter(DayOfWeek.MONDAY, monthDay.atYear(year));
  }

  @Override
  public Map<String, DateFunction> create(SubDivision subDivision) {
    return Collections.emptyMap();
  }

  @Override
  public Map<String, DateFunction> create(Locode locale) {
    return Collections.emptyMap();
  }

  @Override
  public final Locale getCountry() {
    return Locale.US;
  }
}
