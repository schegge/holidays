package de.schegge.holidays.location;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

public enum UnitedKingdomCountryOrProvince implements SubDivision {
  ENG("England"),
  NIR("Northern Ireland"),
  SCT("Scotland"),
  WLS("Wales");

  private final String name;

  UnitedKingdomCountryOrProvince(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getIso() {
    return "GB-" + name();
  }

  @Override
  public Locale getCountry() {
    return Locale.UK;
  }

  public static UnitedKingdomCountryOrProvince byIso(String iso) {
    if (Objects.requireNonNull(iso).length() != 6) {
      throw new IllegalArgumentException();
    }
    String name = iso.substring(3);
    return Arrays.stream(values()).filter(value -> value.name().equals(name)).findFirst().orElse(null);
  }

}
