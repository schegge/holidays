package de.schegge.holidays.location;

public interface Locode {
	String getLocode();

	String getName();

	String getNameWithoutDiacretics();

	SubDivision getSubDivision();
}
