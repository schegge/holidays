package de.schegge.holidays.location;

import java.util.Locale;

public enum SwissCanton implements SubDivision {
	AG("Aargau"), AR("Appenzell Ausserrhoden"), AI("Appenzell Innerrhoden"), BL("Basel-Landschaft"), BS("Basel-Stadt"),
	BE("Bern"), FR("Freiburg"), GE("Genf"), GL("Glarus"), GR("Graubünden"), JU("Jura"), LU("Luzern"), NE("Neuenburg"),
	NW("Nidwalden"), OW("Obwalden"), SH("Schaffhausen"), SZ("Schwyz"), SO("Solothurn"), SG("St. Gallen"), TI("Tessin"),
	TG("Thurgau"), UR("Uri"), VD("Waadt"), VS("Wallis"), ZG("Zug"), ZH("Zürich");

	private String name;

	private SwissCanton(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getIso() {
		return "CH-" + name();
	}

	@Override
	public Locale getCountry() {
		return new Locale("de", "CH");
	}
}
