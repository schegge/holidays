package de.schegge.holidays.location;

import java.util.Locale;

public interface SubDivision {
	String getName();

	String getIso();

	Locale getCountry();
}
