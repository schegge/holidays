package de.schegge.holidays.location;

import java.util.Locale;
import java.util.Objects;

public enum GermanFederalState implements SubDivision {
	BW("Baden-Württemberg"), BY("Bayern"), BE("Berlin"), BB("Brandenburg"), HB("Bremen"), HH("Hamburg"), HE("Hessen"),
	MV("Mecklenburg-Vorpommern"), NI("Niedersachsen"), NW("Nordrhein-Westfalen"), RP("Rheinland-Pfalz"), SL("Saarland"),
	SN("Sachsen"), ST("Sachsen-Anhalt"), SH("Schleswig-Holstein"), TH("Thüringen");

	private final String name;

	GermanFederalState(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getIso() {
		return "DE-" + name();
	}

	@Override
	public Locale getCountry() {
		return Locale.GERMANY;
	}

	public static GermanFederalState byIso(String iso) {
		return valueOf(Objects.requireNonNull(iso).substring(3));
	}
}
