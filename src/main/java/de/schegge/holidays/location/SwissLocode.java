package de.schegge.holidays.location;

public enum SwissLocode implements Locode {
	BRN("Bern");

	private String name;

	private SwissLocode(String name) {
		this.name = name;
	}

	@Override
	public String getLocode() {
		return "CH " + name();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getNameWithoutDiacretics() {
		return name;
	}

	@Override
	public SubDivision getSubDivision() {
		return SwissCanton.BE;
	}
}
