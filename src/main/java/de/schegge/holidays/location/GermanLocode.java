package de.schegge.holidays.location;

import java.util.Objects;

public enum GermanLocode implements Locode {
	AGB("Augsburg", GermanFederalState.BY), BFE("Bielefeld", GermanFederalState.NW),
	BOY("Bad Oeynhausen", GermanFederalState.NW);

	private String name;
	private String nameWithoutDiacretics;
	private GermanFederalState subDivision;

	GermanLocode(String name, GermanFederalState subDivision) {
		this.name = name;
		this.subDivision = subDivision;
		nameWithoutDiacretics = name;
	}

	@Override
	public String getLocode() {
		return "DE " + name();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getNameWithoutDiacretics() {
		return nameWithoutDiacretics;
	}

	@Override
	public GermanFederalState getSubDivision() {
		return subDivision;
	}

	public static GermanLocode byLocode(String locode) {
		return valueOf(Objects.requireNonNull(locode).substring(3));
	}
}
