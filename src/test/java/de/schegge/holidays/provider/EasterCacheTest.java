package de.schegge.holidays.provider;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import de.schegge.holidays.provider.EasterCache;

class EasterCacheTest {
	@Test
	void testEaster1900() {
		assertEquals(LocalDate.of(1900, 4, 15), EasterCache.gauss(1900));
	}
}
