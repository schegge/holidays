package de.schegge.holidays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import de.schegge.holidays.location.UnitedKingdomCountryOrProvince;
import de.schegge.holidays.location.UnitedKingdomNation;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import de.schegge.holidays.location.SwissCanton;
import de.schegge.holidays.location.SwissLocode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class HolidaysTest {

  @Test
  void isHolidaysInUnsupportedLocale() {
    assertThrows(IllegalArgumentException.class, () -> Holidays.in(Locale.CANADA));
  }

  @Test
  void isHolidaysInUnsupportedSubDivision() {
    assertThrows(IllegalArgumentException.class, () -> Holidays.in(SwissCanton.UR));
  }

  @Test
  void holidaysInUnsupportedLocode() {
    assertThrows(IllegalArgumentException.class, () -> Holidays.in(SwissLocode.BRN));
  }

  @Test
  void isHolidaysInAustria() {
    Holidays holidays = Holidays.in(Holidays.AUSTRIA);
    assertTrue(holidays.isHoliday(LocalDate.of(2020, 12, 25)));
    assertTrue(holidays.isHoliday(LocalDate.of(2020, 12, 26)));
    assertTrue(holidays.isHoliday(LocalDate.of(2020, 5, 1)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 8, 24)));
  }

  @Test
  void getHolidaysInLocaleUK() {
    Holidays holidays = Holidays.in(Locale.UK, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 4, 13)));
  }

  @Test
  void getHolidaysInUnitedKingdom() {
    Holidays holidays = Holidays.in(UnitedKingdomNation.UKM, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 8, 31)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 4, 13)));
  }

  @Test
  void getHolidaysInGreatBritain() {
    Holidays holidays = Holidays.in(UnitedKingdomNation.GBN, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 8, 31)));
  }

  @Test
  void getHolidaysInEnglandAndWales() {
    Holidays holidays = Holidays.in(UnitedKingdomNation.EAW, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertEquals(Optional.of("Summer Bank Holiday"), holidays.getHoliday(LocalDate.of(2020, 8, 31)));
    assertEquals(Optional.of("Easter Monday"), holidays.getHoliday(LocalDate.of(2020, 4, 13)));
  }

  @Test
  void getHolidaysInEngland() {
    Holidays holidays = Holidays.in(UnitedKingdomCountryOrProvince.ENG, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertEquals(Optional.of("Summer Bank Holiday"), holidays.getHoliday(LocalDate.of(2020, 8, 31)));
    assertEquals(Optional.of("May Day"), holidays.getHoliday(LocalDate.of(2020, 5, 4)));
    assertEquals(Optional.of("Spring Bank Holiday"), holidays.getHoliday(LocalDate.of(2020, 5, 25)));
    assertEquals(Optional.of("Easter Monday"), holidays.getHoliday(LocalDate.of(2020, 4, 13)));

  }

  @Test
  void getHolidaysInWales() {
    Holidays holidays = Holidays.in(UnitedKingdomCountryOrProvince.WLS, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertEquals(Optional.of("Summer Bank Holiday"), holidays.getHoliday(LocalDate.of(2020, 8, 31)));
    assertEquals(Optional.of("Easter Monday"), holidays.getHoliday(LocalDate.of(2020, 4, 13)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 5, 4)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 5, 25)));
  }

  @Test
  void getHolidaysInScotland() {
    Holidays holidays = Holidays.in(UnitedKingdomCountryOrProvince.SCT, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertEquals(Optional.of("Summer Bank Holiday"), holidays.getHoliday(LocalDate.of(2020, 8, 3)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 8, 31)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 5, 4)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 5, 25)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 4, 13)));
  }

  @Test
  void getHolidaysInNorthernIreland() {
    Holidays holidays = Holidays.in(UnitedKingdomCountryOrProvince.NIR, Locale.UK);
    assertEquals(Optional.of("Christmas Day"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
    assertEquals(Optional.of("Boxing Day"), holidays.getHoliday(LocalDate.of(2020, 12, 26)));
    assertEquals(Optional.of("Good Friday"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
    assertEquals(Optional.of("New Year's Day"), holidays.getHoliday(LocalDate.of(2019, 1, 1)));
    assertEquals(Optional.of("Summer Bank Holiday"), holidays.getHoliday(LocalDate.of(2020, 8, 31)));
    assertEquals(Optional.of("St. Patrick's Day"), holidays.getHoliday(LocalDate.of(2020, 3, 17)));
    assertEquals(Optional.of("Easter Monday"), holidays.getHoliday(LocalDate.of(2020, 4, 13)));
    assertEquals(Optional.of("Orangeman's Day"), holidays.getHoliday(LocalDate.of(2020, 7, 12)));

    assertFalse(holidays.isHoliday(LocalDate.of(2020, 5, 4)));
    assertFalse(holidays.isHoliday(LocalDate.of(2020, 5, 25)));
  }

  @ParameterizedTest
  @CsvSource({
      "New Year's Day, 2019-01-01",
      "New Year's Day, 2011-01-03",
      "St. Patrick's Day, 2020-03-17",
      "May Day, 2020-05-04",
      "August Day, 2020-08-03",
      "October Bank Holiday, 2020-10-26",
      "Christmas Day, 2020-12-25",
  })
  void getHolidaysInIrelandInEnIE(String name, LocalDate date) {
		assertEquals(Optional.of(name), Holidays.in("IE", Holidays.IRELAND).getHoliday(date));
		assertEquals(Optional.of(name), Holidays.in("IE", Locale.US).getHoliday(date));
		assertEquals(Optional.of(name), Holidays.in("IE", Locale.ENGLISH).getHoliday(date));
		assertEquals(Optional.of(name), Holidays.in("IE", Locale.UK).getHoliday(date));
  }

  @Test
	void getBoxingdayInIreland() {
  	LocalDate date = LocalDate.of(2020, 12, 26);
		assertEquals(Optional.of("St. Stephans Day"), Holidays.in("IE", Holidays.IRELAND).getHoliday(date));
		assertEquals(Optional.of("Boxing Day"), Holidays.in("IE", Locale.US).getHoliday(date));
		assertEquals(Optional.of("Boxing Day"), Holidays.in("IE", Locale.ENGLISH).getHoliday(date));
		assertEquals(Optional.of("Boxing Day"), Holidays.in("IE", Locale.UK).getHoliday(date));
	}

	@ParameterizedTest
	@CsvSource({
			"Neujahr, 2019-01-01",
			"Neujahr, 2011-01-03",
			"St. Patrick's Day, 2020-03-17",
			"Maifeiertag, 2020-05-04",
			"Augustfeiertag, 2020-08-03",
			"Oktoberfeiertag, 2020-10-26",
			"1. Weihnachtstag, 2020-12-25",
			"2. Weihnachtstag, 2020-12-26",
	})
	void getHolidaysInIrelandInDeDe(String name, LocalDate date) {
		assertEquals(Optional.of(name), Holidays.in("IE", Locale.GERMANY).getHoliday(date));
		assertEquals(Optional.of(name), Holidays.in("IE", Locale.GERMAN).getHoliday(date));
	}

	@ParameterizedTest
  @CsvSource({
      "New Year's Day, 2019-01-01",
      "Independence Day, 2019-07-04",
      "Veterans Day, 2019-11-11",
      "Christmas Day, 2019-12-25",
  })
  void getHolidaysInUSAInEnUs(String name, LocalDate date) {
    assertEquals(Optional.of(name), Holidays.in("US", Locale.US).getHoliday(date));
		assertEquals(Optional.of(name), Holidays.in("US", Locale.ENGLISH).getHoliday(date));
		assertEquals(Optional.of(name), Holidays.in("US", Locale.UK).getHoliday(date));
		assertEquals(Optional.of(name), Holidays.in("US", Holidays.IRELAND).getHoliday(date));
  }

  @ParameterizedTest
  @CsvSource({
      "Neujahr, 2019-01-01",
      "Unabhängigkeitstag, 2019-07-04",
      "Veteranentag, 2019-11-11",
      "1. Weihnachtstag, 2019-12-25",
  })
  void getHolidaysInUSAInDeDe(String name, LocalDate date) {
    assertEquals(Optional.of(name), Holidays.in("US", Locale.GERMANY).getHoliday(date));
    assertEquals(Optional.of(name), Holidays.in("US", Locale.GERMAN).getHoliday(date));
  }
}
