package de.schegge.holidays;

import ftl.ParseException;
import org.freshmarker.Configuration;
import org.freshmarker.Configuration.TemplateBuilder;
import org.freshmarker.Template;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HolidayInterpolationTest {

    private Configuration configuration;
    private TemplateBuilder templateBuilder;

    @BeforeEach
    public void setUp() {
        configuration = new Configuration();
        configuration.builder().withLocale(Locale.UK);
    }

    @Test
    void interpolationIsHoliday() throws ParseException {
        Template template = configuration.builder().withLocale(Locale.UK).getTemplate("test", "holiday: ${temporal?is_holiday}");
        String result = template.process(Map.of("temporal", LocalDate.of(2022, Month.DECEMBER, 25)));
        assertEquals("holiday: yes", result);
    }

    @Test
    void interpolationGetHolidayEn() throws ParseException {
        Template template = configuration.builder().withLocale(Locale.UK).getTemplate("test", "holiday: ${temporal?get_holiday}");
        String result = template.process(Map.of("temporal", LocalDate.of(2022, Month.DECEMBER, 25)));
        assertEquals("holiday: Christmas Day", result);
    }

    @Test
    void interpolationGetHolidayDe() throws ParseException {
        Template template = configuration.builder().withLocale(Locale.GERMANY).getTemplate("test", "holiday: ${temporal?get_holiday}");
        String result = template.process(Map.of("temporal", LocalDate.of(2022, Month.DECEMBER, 25)));
        assertEquals("holiday: 1. Weihnachtstag", result);
    }

    @Test
    void interpolationGetHolidayDeSetting() throws ParseException {
        Template template = configuration.builder().withLocale(Locale.UK).getTemplate("test", "holiday: ${temporal?get_holiday}<#setting locale='de-DE'> - ${temporal?get_holiday}");
        String result = template.process(Map.of("temporal", LocalDate.of(2022, Month.DECEMBER, 25)));
        assertEquals("holiday: Christmas Day - 1. Weihnachtstag", result);
    }

    @Test
    void interpolationGetHolidayWithFallback() throws ParseException {
        Template template = configuration.builder().withLocale(Locale.UK).getTemplate("test", "holiday: ${temporal?get_holiday('no holiday')}");
        String result = template.process(Map.of("temporal", LocalDate.of(1968, Month.AUGUST, 24)));
        assertEquals("holiday: no holiday", result);
    }
}

