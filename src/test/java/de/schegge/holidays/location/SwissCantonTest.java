package de.schegge.holidays.location;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class SwissCantonTest {
	@ParameterizedTest
	@CsvSource({ //
			"AG, Aargau, CH-AG", //
			"AR, Appenzell Ausserrhoden, CH-AR", //
			"AI, Appenzell Innerrhoden, CH-AI", //
			"BL, Basel-Landschaft, CH-BL", //
			"BS, Basel-Stadt, CH-BS", //
			"BE, Bern, CH-BE", //
			"FR, Freiburg, CH-FR", //
			"GE, Genf, CH-GE", //
			"GL, Glarus, CH-GL", //
			"GR, Graubünden, CH-GR", //
			"JU, Jura, CH-JU", //
			"LU, Luzern, CH-LU", //
			"NE, Neuenburg, CH-NE", //
			"NW, Nidwalden, CH-NW", //
			"OW, Obwalden, CH-OW", //
			"SH, Schaffhausen, CH-SH", //
			"SZ, Schwyz, CH-SZ", //
			"SO, Solothurn, CH-SO", //
			"SG, St. Gallen, CH-SG", //
			"TI, Tessin, CH-TI", //
			"TG, Thurgau, CH-TG", //
			"UR, Uri, CH-UR", //
			"VD, Waadt, CH-VD", //
			"VS, Wallis, CH-VS", //
			"ZG, Zug, CH-ZG", //
			"ZH, Zürich, CH-ZH", //
	})
	void cantons(SwissCanton canton, String name, String iso) {
		assertEquals(name, canton.getName());
		assertEquals(iso, canton.getIso());
	}
}
