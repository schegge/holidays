package de.schegge.holidays.location;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class GermanFederalStateTest {
	@ParameterizedTest
	@CsvSource({ //
			"BW, Baden-Württemberg, DE-BW", //
			"BY, Bayern, DE-BY", //
			"BE, Berlin, DE-BE", //
			"BB, Brandenburg, DE-BB", //
			"HB, Bremen, DE-HB", //
			"HH, Hamburg, DE-HH", //
			"HE, Hessen, DE-HE", //
			"MV, Mecklenburg-Vorpommern, DE-MV", //
			"NI, Niedersachsen, DE-NI", //
			"NW, Nordrhein-Westfalen, DE-NW", //
			"RP, Rheinland-Pfalz, DE-RP", //
			"SL, Saarland, DE-SL", //
			"SN, Sachsen, DE-SN", //
			"ST, Sachsen-Anhalt, DE-ST", //
			"SH, Schleswig-Holstein, DE-SH", //
			"TH, Thüringen, DE-TH" //
	})
	void states(GermanFederalState state, String name, String iso) {
		assertEquals(name, state.getName());
		assertEquals(iso, state.getIso());
	}
}
