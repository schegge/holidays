package de.schegge.holidays.location;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class GermanLocodeTest {
	@Test
	void states() {
		assertEquals("Bielefeld", GermanLocode.BFE.getName());
		assertEquals("DE BFE", GermanLocode.BFE.getLocode());
		assertEquals(GermanFederalState.NW, GermanLocode.BFE.getSubDivision());
		assertEquals("Bad Oeynhausen", GermanLocode.BOY.getName());
		assertEquals("DE BOY", GermanLocode.BOY.getLocode());
		assertEquals(GermanFederalState.NW, GermanLocode.BOY.getSubDivision());
		assertEquals("Augsburg", GermanLocode.AGB.getName());
		assertEquals("DE AGB", GermanLocode.AGB.getLocode());
		assertEquals(GermanFederalState.BY, GermanLocode.AGB.getSubDivision());
	}
}
