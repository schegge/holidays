package de.schegge.holidays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import de.schegge.holidays.location.GermanFederalState;
import de.schegge.holidays.location.GermanLocode;
import de.schegge.holidays.location.SwissCanton;
import de.schegge.holidays.location.SwissLocode;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Optional;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class GermanHolidaysTest {

	@ParameterizedTest(name = "{index} => {0}")
	@CsvSource({ //
			"Neujahr,2000-01-01", //
			"Karfreitag, 2021-04-02", //
			"Ostermontag, 2021-04-05", //
			"Tag der Arbeit, 2019-05-01", //
			"Christi Himmelfahrt, 2021-05-13", //
			"Tag der deutschen Einheit, 1954-06-17", //
			"Tag der deutschen Einheit, 1980-06-17", //
			"Tag der deutschen Einheit, 1990-06-17", //
			"1. Weihnachtstag, 2020-12-25", //
			"2. Weihnachtstag, 2020-12-26", //
	})
	void isHolidaysInGermany(String name, LocalDate date) {
		assertTrue(Holidays.in(Locale.GERMANY).isHoliday(date));
	}

	@ParameterizedTest(name = "{index} => {0}")
	@CsvSource({ //
			"Tag der deutschen Einheit, 1950-06-17", //
			"Tag der deutschen Einheit, 1991-06-17", //
			"Tag der Deutschen Einheit, 1984-10-03", //
			"Mein Geburtstag, 1968-08-24", //
	})
	void isHolidaysNotInGermany(String name, LocalDate date) {
		assertFalse(Holidays.in(Locale.GERMANY).isHoliday(date));
	}

	@ParameterizedTest(name = "{index} => {1} in {0}")
	@CsvSource({ //
			"BB, Ostersonntag, 2021-04-04", //
			"HE, Ostersonntag, 2021-04-04", //
			"BB, Pfingstsonntag, 2021-05-23", //
			"HE, Pfingstsonntag, 2021-05-23", //
			"BB, Reformationstag, 2021-10-31", //
			"NI, Reformationstag, 2021-10-31", //
			"SH, Reformationstag, 2021-10-31", //
			"SN, Reformationstag, 2021-10-31", //
			"ST, Reformationstag, 2021-10-31", //
			"TH, Reformationstag, 2021-10-31", //
			"BE, Frauentag, 2019-03-08", //
			"BW, Fronleichnam, 1990-06-14", //
			"BY, Fronleichnam, 1990-06-14", //
			"HE, Fronleichnam, 1990-06-14", //
			"RP, Fronleichnam, 1990-06-14", //
			"SL, Fronleichnam, 1990-06-14", //
			"SN, Buß und Bettag, 2021-11-17", //
			"SN, Buß und Bettag, 2023-11-22", //
	})
	void isHolidaysInGermanFederalState(GermanFederalState state, String name, LocalDate date) {
		assertTrue(Holidays.in(state).isHoliday(date));
	}

	@Test
	void isHolidaysInBielefeld() {
		Holidays holidays = Holidays.in(GermanLocode.BFE);
		assertTrue(holidays.isHoliday(LocalDate.of(2020, 12, 25)));
		assertTrue(holidays.isHoliday(LocalDate.of(2020, 12, 26)));
		assertTrue(holidays.isHoliday(LocalDate.of(2020, 4, 10)));
		assertTrue(holidays.isHoliday(LocalDate.of(2019, 6, 20)));
		assertFalse(holidays.isHoliday(LocalDate.of(2020, 1, 6)));
		assertFalse(holidays.isHoliday(LocalDate.of(2020, 8, 24)));
		assertTrue(Holidays.in(GermanLocode.BFE).isHoliday(LocalDate.of(2020, 12, 25)));
	}

	@Test
	void getHolidaysInAugsburgWithGermanDefaultLocale() {
		Assumptions.assumeTrue(Locale.GERMAN.getLanguage().equals(Locale.getDefault().getLanguage()));
		Holidays holidays = Holidays.in(GermanLocode.AGB);
		assertEquals(Optional.of("1. Weihnachtstag"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
		assertEquals(Optional.of("Karfreitag"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
		assertEquals(Optional.of("Fronleichnam"), holidays.getHoliday(LocalDate.of(2019, 6, 20)));
		assertTrue(Holidays.in(GermanLocode.AGB).isHoliday(LocalDate.of(1800, 8, 8)));
	}

	@Test
	void getHolidaysInAugsburg() {
		Assumptions.assumeFalse(Locale.GERMAN.getLanguage().equals(Locale.getDefault().getLanguage()));
		Holidays holidays = Holidays.in(GermanLocode.AGB, Locale.GERMANY);
		assertEquals(Optional.of("1. Weihnachtstag"), holidays.getHoliday(LocalDate.of(2020, 12, 25)));
		assertEquals(Optional.of("Karfreitag"), holidays.getHoliday(LocalDate.of(2020, 4, 10)));
		assertEquals(Optional.of("Fronleichnam"), holidays.getHoliday(LocalDate.of(2019, 6, 20)));
		assertTrue(Holidays.in(GermanLocode.AGB).isHoliday(LocalDate.of(1800, 8, 8)));
	}
}
